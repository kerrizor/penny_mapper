# penny_mapper

Mapping penny smashing/pressing machine locations

## TODO

Since we only have street addresses (at best) we need to geocode them to get lat/lng in order to do mapping. [alexreisner/geocoder](https://github.com/alexreisner/geocoder)

Once we have lat/lng, we can add GPX conversion. [dougfales/gpx](https://github.com/dougfales/gpx)


