require "HTTParty"
require "nokogiri"
require "pry"

# Scrape PennyCollector.com  for locations of active penny smashing machines
#


html = HTTParty.get(
  "http://209.221.138.252/AreaList.aspx"
)

# Grab all the table rows for states, dropping leading/trailing junk
#
# area_list = Nokogiri::HTML(html).at_css('[id = "StatesList"]').children
# area_list.shift
# area_list.pop

# states = {}

# area_list.each do |raw_row|
#   entries = raw_row.children
#   entries.shift
#   entries.pop

#   entries.each do |entry|
#     state_id = entry.children[1].attributes["href"].text.split("=")[1]

#     states[state_id] = entry.text.strip
#   end
# end

states = {
  "41"=>"Alabama",
   "58"=>"Louisiana",
   "75"=>"Ohio",
   "42"=>"Alaska",
   "59"=>"Maine",
   "76"=>"Oklahoma",
   "43"=>"Arizona",
   "60"=>"Maryland",
   "77"=>"Oregon",
   "44"=>"Arkansas",
   "61"=>"Massachusetts",
   "78"=>"Pennsylvania",
   "45"=>"California",
   "62"=>"Michigan",
   "79"=>"Rhode Island",
   "46"=>"Colorado",
   "63"=>"Minnesota",
   "80"=>"South Carolina",
   "47"=>"Connecticut",
   "64"=>"Mississippi",
   "81"=>"South Dakota",
   "48"=>"Delaware",
   "65"=>"Missouri",
   "82"=>"Tennessee",
   "14"=>"Florida",
   "66"=>"Montana",
   "83"=>"Texas",
   "51"=>"Georgia",
   "67"=>"Nebraska",
   "84"=>"Utah",
   "52"=>"Hawaii",
   "68"=>"Nevada",
   "85"=>"Vermont",
   "15"=>"Idaho",
   "69"=>"New Hampshire",
   "86"=>"Virginia",
   "53"=>"Illinois",
   "70"=>"New Jersey",
   "87"=>"Washington",
   "54"=>"Indiana",
   "71"=>"New Mexico",
   "49"=>"Washington DC",
   "55"=>"Iowa",
   "72"=>"New York",
   "88"=>"West Virginia",
   "56"=>"Kansas",
   "73"=>"North Carolina",
   "89"=>"Wisconsin",
   "57"=>"Kentucky",
   "74"=>"North Dakota",
   "90"=>"Wyoming"
  }

# Scrape an area page to extract individual record IDs
#
html = HTTParty.get(
  "http://209.221.138.252/Locations.aspx?area=77"
)

# Scrape location

doc = Nokogiri::HTML(html)

# Area listing page
#
# 2..? - Individual entries
#
raw_listings = doc.at_css('[id = "DG"]').children

# Drop the first 2 children nodes, as they're formatting
#
2.times { raw_listings.shift }

formatted_listings = {}

# Individual entry children of interest
#
#   1 - Name & address ([0] and [2])
#   2 - Town
#   3 - Design count
#   4 - Link to detail page
#   5 - Updated
#
raw_listings.each do |listing|
  # Ignore any machines marked as "gone" or "outoforder"
  #
  next if listing.attributes["class"]&.value == "gone"
  next if listing.attributes["class"]&.value == "outoforder"

  uri_node = listing.children[4]

  next unless uri_node

  id = uri_node.at_css("a").attributes["href"].value.split("=")[1]

  formatted_listings[id] = {}
  formatted_listings[id]["name"] = listing.children[1].children[0].text
  formatted_listings[id]["address"] = listing.children[1].children[2].text
  formatted_listings[id]["town"] = listing.children[2].text
  formatted_listings[id]["updated"] = listing.children[5].text
end

puts formatted_listings



# Scrape detail page
#
# html = HTTParty.get(
#   "http://209.221.138.252/Details.aspx?location=3920"
# )

# Parse HTML with nokogiri
#
# doc = Nokogiri::HTML(html)

# Detail page children of interest
#
# 0 - Address
# 1 - Phone
# 2 - Website link
# 3..? - Details
#
# doc.css("p")[n]
# binding.pry
